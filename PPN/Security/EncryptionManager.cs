﻿using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using PPN.CLI;

namespace PPN.Security
{
    public class EncryptionManager
    {
        private static EncryptionManager _encryptionManager;

        public X509Certificate2 Certificate { get; private set; }
        private EncryptionManager()
        {
            CLIOptions opts = Program.CliOptions;
            if (opts.CertPath != null && opts.PKeyPath != null)
            {
                Certificate = KeyMaster.ImportCertificate(opts.CertPath, opts.PKeyPath);
            }
            else
            {
                if (!Directory.Exists("comsec")) Directory.CreateDirectory("comsec");
                opts.CertPath = "comsec/cert.crt";
                opts.PKeyPath = "comsec/key.pem";
                if (File.Exists(opts.CertPath))
                {
                    Certificate = KeyMaster.ImportCertificate(opts.CertPath, opts.PKeyPath);
                }
                else
                {
                    Certificate = KeyMaster.CreateSelfSignedAuthX509();
                    KeyMaster.SaveX509(Certificate, opts.CertPath, opts.PKeyPath);
                }
                
            }
        }

        public static EncryptionManager GetInstance()
        {
            if (_encryptionManager == null)
            {
                _encryptionManager = new EncryptionManager();
            }

            return _encryptionManager;
        }
    }
}