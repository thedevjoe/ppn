﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace PPN.Security
{
    public class KeyMaster
    {
        public static X509Certificate2 CreateSelfSignedAuthX509()
        {
            var mname = Environment.MachineName;
            X500DistinguishedName name = new X500DistinguishedName($"CN=PPN-SS-{mname}");
            using (RSA pkey = RSA.Create())
            {
                var request = new CertificateRequest(name, pkey, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
                request.CertificateExtensions.Add(new X509KeyUsageExtension(X509KeyUsageFlags.DigitalSignature | X509KeyUsageFlags.DataEncipherment, true));
                request.CertificateExtensions.Add(new X509EnhancedKeyUsageExtension(new OidCollection {new Oid("1.3.6.1.5.5.7.3.2"), new Oid("1.3.6.1.5.5.7.3.1")}, true));
                var cert = request.CreateSelfSigned(DateTimeOffset.Now, DateTimeOffset.Now.AddYears(1));
                return cert;
            }
        }

        public static void SaveX509(X509Certificate2 cert, string cfile, string kfile)
        {
            string ct = "-----BEGIN CERTIFICATE-----\n" + Convert.ToBase64String(cert.GetRawCertData()) + "\n-----END CERTIFICATE-----";
            string pt = "-----BEGIN RSA PRIVATE KEY-----\n" +
                        Convert.ToBase64String(cert.GetRSAPrivateKey().ExportRSAPrivateKey()) +
                        "\n-----END RSA PRIVATE KEY-----";
            File.WriteAllText(cfile, ct);
            File.WriteAllText(kfile, pt);
        }

        public static X509Certificate2 ImportCertificate(string cfile, string kfile)
        {
            X509Certificate2 cert = new X509Certificate2(cfile);
            RSA rsa = RSA.Create();
            string key = File.ReadAllText(kfile).Replace("-----BEGIN RSA PRIVATE KEY-----", "").Replace("-----END RSA PRIVATE KEY-----", "");
            byte[] b = Convert.FromBase64String(key);
            rsa.ImportRSAPrivateKey(b, out _);
            cert = cert.CopyWithPrivateKey(rsa);
            return cert;
        }
    }
}