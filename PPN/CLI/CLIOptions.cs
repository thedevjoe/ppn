﻿using CommandLine;

namespace PPN.CLI
{
    public class CLIOptions
    {
        [Option('g', "group", Required = false, HelpText = "Allows PPN to run in group/node mode")]
        public bool GroupMode { get; set; }
        
        [Option("cert", Required = false, HelpText = "The certificate to use for connections. If not set, one is generated.")]
        public string CertPath { get; set; }
        
        [Option("pkey", Required = false, HelpText = "The private key to use for connections")]
        public string PKeyPath { get; set; }
    }
}