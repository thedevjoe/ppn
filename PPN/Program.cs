﻿using System;
using System.Diagnostics;
using System.IO;
using CommandLine;
using PPN.CLI;
using PPN.Networking;
using PPN.Security;
using PPN.Transport;
using PPN.Transport.UDP;
using PPN.Transport.UDP.V1;

namespace PPN
{
    class Program
    {
        public static CLIOptions CliOptions { get; private set; }
        public static ushort[] Address { get; private set; }
        public static GroupInformation GroupInfo;
        static void Main(string[] args)
        {
            var parse = CommandLine.Parser.Default.ParseArguments<CLIOptions>(args);
            if (parse.Tag == ParserResultType.NotParsed) return;
            CliOptions = parse.Value;
            Console.WriteLine("Private Pipe Network\nCreated by Joe Longendyke\n-------------------");
            Console.WriteLine("Beginning Initialization");
            EncryptionManager emanager = EncryptionManager.GetInstance();
            if (emanager.Certificate.Issuer == emanager.Certificate.Subject)
            {
                Console.WriteLine("--WARNING-- Using a self signed certificate may reduce your ability to join groups and clusters.");
            }

            Address = new ushort[] {0, 0, emanager.Certificate.CalculateAddressComponent()};
            Console.WriteLine("Current Address: 0.0." + emanager.Certificate.CalculateAddressComponent());
            EndServer.StartEndServer(new UdpServer());
            var ping = new PingPayload(true);
            var pack = TransportManager.PrepareForTransport(ping.Compile(), 1, new ushort[] {0, 0, 5},
                new ushort[] {48973, 1123, 6335});
            Debug.WriteLine(BitConverter.ToString(pack).Replace("-", ""));
        }
    }
}