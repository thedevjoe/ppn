﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using PPN.Security;
using PPN.Transport.UDP.V1;

namespace PPN.Transport
{
    public class TransportManager
    {
        private static TransportManager _transportManager;
        private Dictionary<ulong, Dictionary<ushort, Type>> _packetTypes = new Dictionary<ulong, Dictionary<ushort, Type>>();
        private TransportManager()
        {
            var v1 = new Dictionary<ushort, Type>();
            v1.Add(1, typeof(PingPayload));
            
            _packetTypes.Add(1, v1);
        }

        public static TransportManager GetInstance()
        {
            if(_transportManager == null) _transportManager = new TransportManager();
            return _transportManager;
        }

        public IPayload NewPayloadForPacket(IPacket packet)
        {
            Type t = _packetTypes[packet.GetVersion()][packet.GetPacketType()];
            ConstructorInfo inf = t.GetConstructor(new Type[] { typeof(byte[]) });
            IPayload pack = (IPayload)inf?.Invoke(new object[] {packet.GetRawPayload()});
            return pack;
        }

        public static byte[] PrepareForTransport(byte[] bytes, ushort type, ushort[] from, ushort[] to)
        {
            Random r = new Random();
            // Header = 70 bytes
            byte[] fullpack = new byte[286 + bytes.Length];
            using(MemoryStream stream = new MemoryStream(fullpack))
            using (BinaryWriter writer = new BinaryWriter(stream))
            {
                writer.Write((ulong)1);
                writer.Write(type);
                foreach(ushort s in from)
                {
                    writer.Write(s);
                }

                foreach (ushort s in to)
                {
                    writer.Write(s);
                }
                writer.Write((ulong)r.Next(0, Int32.MaxValue));
                byte[] sig = EncryptionManager.GetInstance().Certificate.GetRSAPrivateKey()
                    .SignData(bytes, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
                writer.Write(sig);
                writer.Write(bytes);
            }

            return fullpack;
        }
    }
}