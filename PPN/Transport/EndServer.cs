﻿using System;
using System.IO;
using System.Net;
using System.Threading;

namespace PPN.Transport
{
    public abstract class EndServer
    {
        public int Port { get; private set; }
        protected Thread RunningThread;
        protected EndServer(int port)
        {
            Port = port;
        }

        public abstract void Listen();
        public abstract void Stop();

        public static Thread StartEndServer(EndServer server)
        {
            if (server.RunningThread != null) return null;
            Thread thr = new Thread(new ThreadStart(server.Listen));
            thr.Start();
            server.RunningThread = thr;
            return thr;
        }

        public abstract void SendData(byte[] b, IPEndPoint ep, OnReply callback);

        protected ulong GetNonceFromArray(byte[] b)
        {
            using(MemoryStream str = new MemoryStream(b))
            using (BinaryReader reader = new BinaryReader(str))
            {
                str.Position = 22;
                return reader.ReadUInt64();
            }
        }

        public abstract void ProcessReplies(ulong nonce, IPayload payload);
        
        public delegate void OnReply(IPayload payload);

        public struct TransmittedInformation
        {
            public DateTime SentAt;
            public ulong Nonce;
            public OnReply Callback;
        }
    }
}