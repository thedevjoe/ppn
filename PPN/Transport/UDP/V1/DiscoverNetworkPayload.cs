﻿using System.IO;
using System.Net;

namespace PPN.Transport.UDP.V1
{
    public class DiscoverNetworkPayload : IPayload
    {
        private bool _response = false;
        private ushort _nodeType;
        public DiscoverNetworkPayload(byte[] bytes)
        {
            using(MemoryStream str = new MemoryStream(bytes))
            using (BinaryReader br = new BinaryReader(str))
            {
                _response = br.ReadUInt16() == 1;
                _nodeType = br.ReadUInt16();
            }
        }

        public DiscoverNetworkPayload()
        {
            
        }

        public DiscoverNetworkPayload(ushort nodetype)
        {
            _response = true;
            _nodeType = nodetype;
        }
        public void Execute(EndServer server, IPEndPoint point, IPacket pack)
        {
            if (_response)
            {
                server.ProcessReplies(pack.GetNonce(), this);
            }
            else
            {
                DiscoverNetworkPayload pl = new DiscoverNetworkPayload(0);
                byte[] bts = TransportManager.PrepareForTransport(pl.Compile(), 1, Program.Address, pack.GetOrigin());
                server.SendData(bts, point, null);
            }
        }

        public byte[] Compile()
        {
            byte[] buffer = new byte[(_response ? 4 : 2)];
            using(MemoryStream str = new MemoryStream(buffer))
            using (BinaryWriter wr = new BinaryWriter(str))
            {
                wr.Write((ushort)(_response ? 1 : 0));
                if(_response) wr.Write((ushort)_nodeType);
            }

            return buffer;
        }
    }
}