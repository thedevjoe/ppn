﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace PPN.Transport.UDP.V1
{
    public class PingPayload : IPayload
    {
        private byte[] _bytes;
        
        public bool Response;
        
        public PingPayload(byte[] bytes)
        {
            _bytes = bytes;
            using(MemoryStream stream = new MemoryStream(_bytes))
            using (BinaryReader reader = new BinaryReader(stream))
            {
                Response = reader.ReadUInt16() == 0;
            }
        }

        public PingPayload(bool response)
        {
            Response = response;
        }
        
        public void Execute(EndServer server, IPEndPoint ep, IPacket pack)
        {
            Debug.WriteLine((Response ? "Response" : "Not Response"));
            if (!Response)
            {
                byte[] b = new PingPayload(true).Compile();
                server.SendData(TransportManager.PrepareForTransport(b, 1, Program.Address, pack.GetOrigin()), ep, null);
            }
            else
            {
                server.ProcessReplies(pack.GetNonce(), this);
            }
        }

        public byte[] Compile()
        {
            byte[] buffer = new byte[4];
            using(MemoryStream s = new MemoryStream(buffer))
            using (BinaryWriter w = new BinaryWriter(s))
            {
                w.Write((ushort)1);
            }

            return buffer;
        }
    }
}