﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace PPN.Transport.UDP
{
    public class UdpServer : EndServer
    {
        private UdpClient _client;
        public UdpServer() : base(9741)
        {
            _client = new UdpClient(this.Port);
        }

        public override void Listen()
        {
            Console.WriteLine("UDPServer is listening");
            while (true)
            {
                IPEndPoint returnTo = null;
                byte[] dgram = _client.Receive(ref returnTo);
                UdpPacket pack = null;
                bool success = UdpPacket.TryParsePacket(dgram, out pack);
                if (!success) continue;
                ushort[] dest = pack.GetDestination();
                if (!dest.SequenceEqual(Program.Address) && !Program.CliOptions.GroupMode)
                {
                    Console.WriteLine("--WARNING-- Received a packet from physical address " + returnTo.Address.ToString() + " that was not intended for us.");
                    Console.WriteLine("-- -- -- -- Intended Destination: " + dest[0] + "." + dest[1] + "." + dest[2]);
                    Console.WriteLine("-- -- -- -- Actual Destination: " + Program.Address[0] + "." + Program.Address[1] + "." + Program.Address[2]);
                    return;
                }
                try
                {
                    pack.GetPayload().Execute(this, returnTo, pack);
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Fail while parsing additional packet data", e.ToString());
                }
                foreach(ulong i in Callbacks.Keys.ToArray())
                {
                    var inf = Callbacks[i];
                    if (inf.SentAt < DateTime.Now.AddMinutes(1)) Callbacks.Remove(i);
                }
            }
        }

        public override void Stop()
        {
            RunningThread?.Abort();
        }
        
        private Dictionary<ulong, TransmittedInformation> Callbacks = new Dictionary<ulong, TransmittedInformation>();

        public override void SendData(byte[] b, IPEndPoint ep, OnReply callback)
        {
            int i = _client.Send(b, b.Length, ep);
            if(callback != null) Callbacks.Add(GetNonceFromArray(b), new TransmittedInformation()
            {
                Callback = callback,
                Nonce = GetNonceFromArray(b),
                SentAt = DateTime.Now
            });
        }

        public override void ProcessReplies(ulong nonce, IPayload payload)
        {
            Callbacks[nonce].Callback.Invoke(payload);
        }
    }
}