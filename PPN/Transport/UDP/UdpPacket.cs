﻿using System;
using System.Diagnostics;
using System.IO;

namespace PPN.Transport.UDP
{
    public class UdpPacket : IPacket
    {
        private byte[] _rawBytes;
        private ushort _packetType;
        private ulong _version;
        private ushort[] _destination;
        private ushort[] _origin;
        private ulong _nonce;
        private byte[] _signature;
        public UdpPacket(byte[] bytes)
        {
            _rawBytes = bytes;
            using(MemoryStream stream = new MemoryStream(bytes))
            using(BinaryReader reader = new BinaryReader(stream))
            {
                _version = reader.ReadUInt64();
                _packetType = reader.ReadUInt16();
                _origin = new ushort[] {reader.ReadUInt16(), reader.ReadUInt16(), reader.ReadUInt16()};
                _destination = new ushort[] {reader.ReadUInt16(), reader.ReadUInt16(), reader.ReadUInt16()};
                _nonce = reader.ReadUInt64();
                _signature = new byte[256];
                for (int i = 0; i < 256; i++)
                {
                    _signature[i] = reader.ReadByte();
                }
            }
        }

        public static bool TryParsePacket(byte[] bytes, out UdpPacket pack)
        {
            try
            {
                pack = new UdpPacket(bytes);
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception in TryParsePacket", e.ToString());
                pack = null;
                return false;
            }
        }
        
        public ushort GetPacketType()
        {
            return _packetType;
        }

        public ulong GetVersion()
        {
            return _version;
        }

        public ushort[] GetDestination()
        {
            return _destination;
        }

        public ushort[] GetOrigin()
        {
            return _origin;
        }

        public ulong GetNonce()
        {
            return _nonce;
        }

        public byte[] GetSignature()
        {
            return _signature;
        }

        public byte[] GetRawPayload()
        {
            return _rawBytes;
        }

        public IPayload GetPayload()
        {
            return TransportManager.GetInstance().NewPayloadForPacket(this);
        }
    }
}