﻿namespace PPN.Transport
{
    public interface IPacket
    {
        public ushort GetPacketType();
        public ulong GetVersion();
        public ushort[] GetDestination();
        public ushort[] GetOrigin();
        public ulong GetNonce();
        public byte[] GetSignature();
        public byte[] GetRawPayload();
        public IPayload GetPayload();
    }
}