﻿using System.Net;

namespace PPN.Transport
{
    public interface IPayload
    {
        public void Execute(EndServer server, IPEndPoint point, IPacket pack);
        public byte[] Compile();
    }
}