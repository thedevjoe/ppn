﻿using System.Net;

namespace PPN.Networking
{
    public struct GroupInformation
    {
        public short AddressComponent;
        public IPEndPoint PhysicalAddress;
        public long LastContact;
    }
}