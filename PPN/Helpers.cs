﻿using System.Security.Cryptography.X509Certificates;

namespace PPN
{
    public static class Helpers
    {
        public static ushort CalculateAddressComponent(this X509Certificate cert)
        {
            ulong constr = 0;
            foreach (byte b in cert.GetRawCertData())
            {
                constr += b;
            }

            constr %= 65530;
            return (ushort)constr;
        }
    }
}